import {Entity,Column,PrimaryGeneratedColumn  } from "typeorm";
@Entity()
export class issueBook{
    @PrimaryGeneratedColumn("uuid")
    uuid:string;

    @Column()
    user_id:string;
    
    @Column()
     book_id:string;

    @Column()
    bookName:string;

    @Column()
    issue_Date:Date;

    @Column()
    return_Date:Date;
}