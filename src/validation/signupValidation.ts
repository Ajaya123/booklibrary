import { Joi } from "express-validation";
export const signupValidation = {
    body: Joi.object({
        name: Joi.string()
            .required(),
        email: Joi.string()
            .email()
            .required(),
        phone: Joi.string()
            .min(10)
            .max(10)
            .required(),
        role: Joi.string()
            .required()
    }),
}