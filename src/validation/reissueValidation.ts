import { Joi } from "express-validation";
export const reissueValidation = {
    body: Joi.object({
        uuid: Joi.string()
            .required(),
        user_id: Joi.string()
            .required(),
    }),
}