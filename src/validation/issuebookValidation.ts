import { Joi } from "express-validation";
export const issuebookValidation = {
    body: Joi.object({
        id: Joi.string()
            .required(),
        bookName: Joi.string()
            .required(),
    }),
}