import { Joi } from "express-validation";
export const updatebookValidation = {
    body: Joi.object({
        id:Joi.string()
            .required(),
        bookName: Joi.string()
            .required(),
        availability: Joi.string()
            .required()

    }),
}