import { Joi } from "express-validation";
export const addbookValidation = {
    body: Joi.object({
        bookName: Joi.string()
            .required(),
        availability: Joi.string()
            .required()

    }),
}