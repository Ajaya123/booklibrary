import {UserController} from "./controller/UserController";
import {BookController  } from "./controller/BookController";
//import { Check } from "typeorm";
export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/signup",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove"
},
{
    method: "post",
route: "/issuebook/:id",
controller:UserController,
action: "issuebook"
},{
    method: "get",
    route: "/viewissue/:id",
    controller: UserController,
    action: "viewIssueBook"
},
{
    method: "patch",
    route: "/reissue",
    controller: UserController,
    action: "reIssueBook"
},
{
    method: "delete",
    route: "/returnbook/:user_id",
    controller: UserController,
    action: "returnBook"
}];

export const BookRoutes = [{
    method: "get",
    route: "/signup/book",
    controller:BookController,
    action: "all"
}, {
    method: "get",
    route: "/book/:id",
    controller:BookController,
    action: "one"
}, {
    method: "post",
    route: "/signup/addbook/:id",
    controller: BookController,
    action: "save"
}, {
    method: "delete",
    route: "/signup/deletebook/:idd",
    controller:BookController,
    action: "remove"
},{
    method: "patch",
    route: "/signup/updatebook/:idd",
    controller:BookController,
    action: "update"
},

    ];