import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Book} from "../entity/Book";
import { User } from "../entity/User";
import { getConnection } from "typeorm";

export class BookController {

    //private userRepository = getRepository(Book);

    public static async all(request: Request, response: Response, next: NextFunction) {
        const book=await getRepository(Book).find();
        response.send(book)
    }
//    public static async example(request: Request, response: Response, next: NextFunction) {
//        console.log("..............");

//        // const book=await getRepository(Book).find();
//         response.send(request.body);
//     }
    async one(request: Request, response: Response, next: NextFunction) {
        return getRepository(Book).findOne(request.params.id);
    }

    public static async save(request: Request, response: Response, next: NextFunction) {
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User,"user")
        .where("user.role = :role AND user.id=:id", {role:"admin",id:request.params.id })
        .getOne();
        if(user){
            const book = await getConnection()
        .createQueryBuilder()
        .select("book")
        .from(Book, "book")
        .where("book.bookName = :bookName", { bookName: request.body.bookName })
        .getOne();
                if(book){
                    response.send(JSON.stringify({"message":null, "error": "this book is allready availabe u can't add another"}));
                } else{
                const addbook=await getRepository(Book).save(request.body);
                response.send(addbook);
                }
    }else{
            response.send(JSON.stringify({"messagr":"this is not admin", "error": null}));
            //response.json("this is not admin");
        }
    
    }

    public static async remove(request: Request, response: Response, next: NextFunction) {
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User,"user")
        .where("user.role = :role AND user.id=:idd", {role:"admin",idd:request.params.idd })
        .getOne();
        if(user){
            const book=await getRepository(Book).findOne(request.body.book_id);
            if(book){
            await getRepository(Book).remove(book);
            response.send(JSON.stringify({"message":"successfully deleted", "error": null}));
            }else{
                response.send(JSON.stringify({"message":"Book id not found", "error": null}));
            }
        }else{
            response.send(JSON.stringify({"message":"only admin can delete the book", "error": null}));
            //response.json("this is not admin");
        }
    }
    public static async update(request: Request, response: Response, next: NextFunction) {
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User,"user")
        .where("user.role = :role AND user.id=:idd", {role:"admin",idd:request.params.idd })
        .getOne();
        if(user){
            const book=await getRepository(Book).findOne({
                id:request.body.id,
                bookName:request.body.bookName
            });
            if(book){

               book.availability=request.body.availability;
                const results=await getRepository(Book).save(book);
                return response.json(results);
            }
            else{
                response.send(JSON.stringify({"message":"Book id not found", "error": null}));
            }
        }else{
            response.send(JSON.stringify({"message":"only admin can update the book", "error": null}));
            //response.json("this is not admin");
        }
    }
           
    }

