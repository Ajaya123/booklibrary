import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import {getConnection  } from "typeorm";
import {issueBook} from "../entity/issueBook";
import * as moment from "moment";
import { Book } from "../entity/Book";
import { RequestOptions } from "https";
import { request } from "http";
const express = require('express');
const {body,validationResults} = require('express-validator');
export class UserController {
   // private userRepository = getRepository(User)
    public static async all(request: Request, response: Response, next: NextFunction) {
        const user=await getRepository(User).find();
        response.send(user);
    }
    async one(request: Request, response: Response, next: NextFunction) {
        return getRepository(User).findOne(request.params.id);
    }
     public static async save(request: Request, response: Response, next: NextFunction) {                  
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User,"user")
        .where("user.email = :email OR user.phone=:phone", {email:request.body.email,phone:request.body.phone })
        .getOne();
        if(user){
            response.send(JSON.stringify({"message":null, "error": "email and phone number allready exit."}));
            //response.json("email and phone number allready exit.")
        }else{
        const users=getRepository(User).save(request.body);
        //response.send(users);
        //console.log(users);
        response.send(JSON.stringify({"message":"successfully sigup.", "error": null,"status":users}));
    }
}
    public static async remove(request: Request, response: Response, next: NextFunction) {
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User,"user")
        .where("user.role = :role AND user.id=:idd", {role:"admin",idd:request.params.idd })
        .getOne();
        if(user){
            const user=await getRepository(User).findOne(request.body.user_id);
            if(user){
            await getRepository(User).remove(user);
            response.send(JSON.stringify({"message":"successfully deleted", "error": null}));
            }else{
                response.send(JSON.stringify({"message":"user not found", "error": null}));
            }
        }else{
            response.send(JSON.stringify({"message":"only admin can delete the book", "error": null}));
            //response.json("this is not admin");
        }
    }

    public static async issuebook(request: Request, response: Response, next: NextFunction){
        const user = await getConnection()
        .createQueryBuilder()
        .select("user")
        .from(User, "user")
        .where("user.id = :id ", { id: request.params.id })
        .getOne();
        if(user){
            const vbook = await getConnection()
            .createQueryBuilder()
            .select("vbook")
            .from(Book, "vbook")
            .where("vbook.id=:id AND vbook.bookName = :bookName  ", { id:request.body.id, bookName: request.body.bookName})
            .getOne();
            if(vbook){
        const book = await getConnection()
        .createQueryBuilder()
        .select("book")
        .from(Book, "book")
        .where(" book.bookName = :bookName AND book.availability>0 ", { bookName: request.body.bookName})
        .getOne();
        if(book){
        //     const bookissue = await getConnection()
        // .createQueryBuilder()
        // .select("bookissue")
        // .from(issueBook, "bookissue")
        // .where("bookissue.bookName = :bookName AND bookissue.user_id=:user_id", { bookName: request.body.bookName,user_id:request.body.user_id })
        // .getOne();
        //         if(bookissue){
        //             response.send(JSON.stringify({"message":null, "error": "ypu can't issue same book again"}));

        //         }else{
        const issuebook=new issueBook();
        issuebook.user_id=request.params.id;
        issuebook.book_id=request.body.id;
        issuebook.bookName=request.body.bookName;
        issuebook.issue_Date=moment().toDate();
        
        issuebook.return_Date=moment().add(1,'M').toDate();
        const results=await getRepository(issueBook).save(issuebook);
        const upadateBook=await getConnection()
        .createQueryBuilder()
        .update(Book)
        .set({availability:()=>"availability-1"})
        .where("book.bookName=:bookName",{bookName:request.body.bookName})
        .execute();
        // await getConnection()
        // .createQueryBuilder()
        // .delete()
        // .from(Book)
        // .where("availability = :availability", { availability: 0 })
        // .execute();
        response.send(JSON.stringify({"message":"successfully book issue compelete", "error": null,"result":results}));
        
               // }
        }
        else{
            response.send(JSON.stringify({"message":null, "error": "this  book is not available in book library"}));
        
            }
            }else{
                response.send(JSON.stringify({"message":null, "error": "Enter a valid bookid and valid bookname"}));
            }
    }else{
        response.send(JSON.stringify({"message":null, "error": "this  user is not available in user table"}));

    }
    }
    public static async viewIssueBook(request: Request, response: Response, next: NextFunction) {
        const book=await getRepository(issueBook).findOne({user_id:request.params.user_id});
        if(book){
        const viewissuebook=await getRepository(issueBook).find({user_id:request.params.user_id});
        response.send(viewissuebook);
        }else{
            response.send(JSON.stringify({"message":null, "error": "this  user is not found"}));
    
        }
    }
    public static async reIssueBook(request:Request, response: Response, next: NextFunction){
        const issuebook=await getRepository(issueBook).findOne({
            uuid:request.body.uuid,
            user_id:request.body.user_id
        });
        if(issuebook){
            issuebook.return_Date=moment(issuebook.return_Date).add(1,'M').toDate();
            await getRepository(issueBook).save(issuebook);
        }
        else{
            response.send(JSON.stringify({"message":null, "error":"book with this userId and uuid doesn't exist"}));
        }
   
       
     response.send(JSON.stringify({"message":"successfully book reissued", "error": null}));

    }
    public static async returnBook(request:Request, response: Response, next: NextFunction){

        const issuebook=await getRepository(issueBook).findOne({
            uuid:request.body.uuid,
            user_id:request.params.user_id,
            book_id:request.body.book_id,
            bookName:request.body.bookName
            
        });
        if(issuebook){
            await getRepository(issueBook).remove(issuebook);
           // response.send(JSON.stringify({"message":"successfully book returned", "error": null}));;
            const upadateBook=await getConnection()
            .createQueryBuilder()
            .update(Book)
            .set({availability:()=>"availability+1"})
            .where("book.id=:book_id AND book.bookName=:bookName",{book_id:request.body.book_id,bookName:request.body.bookName})
            .execute();
        }
        else{
            response.send(JSON.stringify({"message":null, "error":"book with this userId and uuid doesn't exist"}));
        }

     response.send(JSON.stringify({"message":"successfully book returned", "error": null}));

    }
}