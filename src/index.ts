import "reflect-metadata";
import {createConnection, getRepository} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
const { check, validationResult} = require('express-validator');
import {Request, Response} from "express";
import {Routes,BookRoutes} from "./routes";
import {User} from "./entity/User";
import * as session from "express-session";
import { getConnection } from "typeorm";
import { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } from "constants";
import { BookController } from "./controller/BookController";
import { UserController } from "./controller/UserController";
import { signupValidation } from "./validation/signupValidation";
import { issuebookValidation } from "./validation/issuebookValidation";
import { reissueValidation } from "./validation/reissueValidation";
import { addbookValidation } from "./validation/addbookValidation";
import { updatebookValidation } from "./validation/updatebookValidation";
const { validate, ValidationError } = require('express-validation')
 

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());
    var jsonParser=bodyParser.json();
    var urlencodedParser=bodyParser.urlencoded({extended:false})
    // register express routes from defined application routes
     app.get('/users', UserController.all);

      app.post('/signup', validate(signupValidation, {}, {}), (req, res,next) => {
        next();
      },UserController.save);
     //issueBook api 
      app.post('/issuebook/:id', validate(issuebookValidation, {}, {}), (req, res,next) => {
        next();
      },UserController.issuebook);
     //viewisueBook
     app.get('/viewissue/:user_id', UserController.viewIssueBook);
     //reissuebook
     app.patch('/reissue', validate(reissueValidation, {}, {}), (req, res,next) => {
        next();
      },UserController.reIssueBook);
      //returnbook
      app.delete('/users/:idd', UserController.remove);
      //show all book
      app.get('/signup/book', BookController.all);
      //add book
      app.post('/signup/addbook/:id', validate(addbookValidation, {}, {}), (req, res,next) => {
        next();
      },BookController.save);
      //delete book
      app.delete('/signup/deletebook/:idd', BookController.remove);
      //update book
      app.patch('/signup/updatebook/:idd', validate(updatebookValidation, {}, {}), (req, res,next) => {
        next();
      },BookController.update);


    app.use(function(err, req, res, next) {
        if (err instanceof ValidationError) {
          return res.status(err.statusCode).json(err)
        }
       
        return res.status(500).json(err)
      })
      app.delete('/returnbook/:user_id',UserController.returnBook);

    // Routes.forEach(route => {
    //     (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
    //         const result = (new (route.controller as any))[route.action](req, res, next);
    //         if (result instanceof Promise) {
    //             result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

    //         } else if (result !== null && result !== undefined) {
    //             res.json(result);
    //         }
    //     });
    // });
    // BookRoutes.forEach(route => {
    //     (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
    //         const result = (new (route.controller as any))[route.action](req, res, next);
    //         if (result instanceof Promise) {
    //             result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

    //         } else if (result !== null && result !== undefined) {
    //             res.json(result);
    //         }
    //     });
    // });
    
    // start express server
    app.listen(3002);

    // insert new users for test
    

    console.log("Express server has started on port 3002. Open http://localhost:3002/users to see results");

}).catch(error => console.log(error));
